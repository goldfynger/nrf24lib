#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "nrf24.h"
#include "nrf24_hal.h"

#include "uart.h"


void ProcessNRF24();


int main(void)
{
    sei();

    HAL_NRF24_Init();

    HAL_UART_Init();

    while(1)
    {
        ProcessNRF24();

        HAL_UART_Process();
    }
}

void ProcessNRF24()
{    
    if (!NRF24_IsInitialized())
    {
        if (NRF24_Start() != NRF24_ERR_OK) return;
    }

    NRF24_StateTypeDef state;
    
    if (NRF24_Process(&state) != NRF24_ERR_OK) return;
    
    if (state & NRF24_STATE_RXCOMPLETE)
    {
        uint8_t width = 0;
    
        if (NRF24_ReadRxPayloadWidth(&width) != NRF24_ERR_OK) return;
    
        if (width)
        {
            uint8_t data[width];
            uint8_t pipe;
            
            if (NRF24_Receive(data, width, &pipe) != NRF24_ERR_OK) return;

            for (uint8_t i = 0; i < width; i++)
            {
                HAL_UART_TxEnqueue(data[i]);
            }
        }
    }
}
