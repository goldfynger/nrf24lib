#include "nrf24.h"
#include "nrf24_hal.h"


/* Common defines */

#define NRF24_POWER_DELAY_MS 2
#define NRF24_TX_DELAY_US 15
#define NRF24_DIR_CHANGE_DELAY_US 150
#define NRF24_CONN_DELAY_MS 1
#define NRF24_CONN_REPEAT_TIMES 100

#define NRF24_DATA_MAX_LENGTH 32

#define NRF24_REGISTER_MASK 0x1F


/* Check defines */

#if (defined(NRF24_MODE_TX_ONLY) && defined(NRF24_MODE_RX_ONLY)) || \
    (defined(NRF24_MODE_TX_ONLY) && defined(NRF24_MODE_RX_TX)) || \
    (defined(NRF24_MODE_RX_ONLY) && defined(NRF24_MODE_RX_TX))
        #error "Only one one mode can be defined."
#endif

#if (!defined(NRF24_MODE_TX_ONLY) && !defined(NRF24_MODE_RX_ONLY) && !defined(NRF24_MODE_RX_TX))
    #error "One of mode must be defined."
#endif

#ifdef NRF24_MODE_RX_TX
    #if (defined(NRF24_RXTX_DIRECT) && defined(NRF24_RXTX_REVERSE))
        #error "Only one direction can be defined."
    #endif

    #if (!defined(NRF24_RXTX_DIRECT) && !defined(NRF24_RXTX_REVERSE))
        #error "One of direction must be defined."
    #endif

    #if (!defined(NRF24_TX_BYTE) && !defined(NRF24_RX_BYTE))
        #error "Only one direction can be defined."
    #endif
#endif

#ifdef NRF24_MODE_TX_ONLY    
    #if (defined(NRF24_DEV_1) & ((defined(NRF24_DEV_2) || defined(NRF24_DEV_3) || defined(NRF24_DEV_4) || defined(NRF24_DEV_5)))) || \
        (defined(NRF24_DEV_2) & ((defined(NRF24_DEV_1) || defined(NRF24_DEV_3) || defined(NRF24_DEV_4) || defined(NRF24_DEV_5)))) || \
        (defined(NRF24_DEV_3) & ((defined(NRF24_DEV_1) || defined(NRF24_DEV_2) || defined(NRF24_DEV_4) || defined(NRF24_DEV_5)))) || \
        (defined(NRF24_DEV_4) & ((defined(NRF24_DEV_1) || defined(NRF24_DEV_2) || defined(NRF24_DEV_3) || defined(NRF24_DEV_5)))) || \
        (defined(NRF24_DEV_5) & ((defined(NRF24_DEV_1) || defined(NRF24_DEV_2) || defined(NRF24_DEV_3) || defined(NRF24_DEV_4))))
            #error "Only one device type can be defined."
    #endif
    
    #if (!defined(NRF24_DEV_1) && !defined(NRF24_DEV_2) && !defined(NRF24_DEV_3) && !defined(NRF24_DEV_4) && !defined(NRF24_DEV_5))
        #error "One of device type must be defined."
    #endif
    
    #if (!defined(NRF24_TX_BYTE) && !defined(NRF24_RX_BYTE))
        #error "Only one direction can be defined."
    #endif
    
    #ifdef NRF24_DEV_1
        #define NRF24_TX_LAST NRF24_PIPE_1
    #endif
    #ifdef NRF24_DEV_2
        #define NRF24_TX_LAST NRF24_PIPE_2
    #endif
    #ifdef NRF24_DEV_3
        #define NRF24_TX_LAST NRF24_PIPE_3
    #endif
    #ifdef NRF24_DEV_4
        #define NRF24_TX_LAST NRF24_PIPE_4
    #endif
    #ifdef NRF24_DEV_5
        #define NRF24_TX_LAST NRF24_PIPE_5
    #endif
#endif

#ifdef NRF24_MODE_RX_ONLY    
    #if (!defined(NRF24_USE_PIPE_1) && !defined(NRF24_USE_PIPE_2) && !defined(NRF24_USE_PIPE_3) && !defined(NRF24_USE_PIPE_4) && !defined(NRF24_USE_PIPE_5))
        #error "One or more of pipes must be defined."
    #endif
    
    #if (!defined(NRF24_TX_BYTE) && !defined(NRF24_RX_BYTE))
        #error "Only one direction can be defined."
    #endif
#endif


#define NRF24_MIN_FREQ 0
#define NRF24_MAX_FREQ 125

#if (NRF24_FREQ < NRF24_MIX_FREQ || NRF24_FREQ > NRF24_MAX_FREQ)
    #error "Frequency channel error."
#endif


/* Commands */

#define NRF24_R_REGISTER          0x00 // Read register command. Used as sum with register value.
#define NRF24_W_REGISTER          0x20 // Write register command. Used as sum with register value.
#define NRF24_R_RX_PAYLOAD        0x61
#define NRF24_W_TX_PAYLOAD        0xA0
#define NRF24_FLUSH_TX            0xE1
#define NRF24_FLUSH_RX            0xE2
#define NRF24_REUSE_TX_PL         0xE3
#define NRF24_R_RX_PL_WID         0x60
#define NRF24_W_ACK_PAYLOAD       0xA8
#define NRF24_W_TX_PAYLOAD_NOACK  0xB0
#define NRF24_NOP                 0xFF // NOP command. Can be used for read status register.


/* Registers */

#define NRF24_CONFIG      0x00
#define NRF24_EN_AA       0x01
#define NRF24_EN_RXADDR   0x02
#define NRF24_SETUP_AW    0x03
#define NRF24_SETUP_RETR  0x04
#define NRF24_RF_CH       0x05
#define NRF24_RF_SETUP    0x06
#define NRF24_STATUS      0x07 
#define NRF24_OBSERVE_TX  0x08
#define NRF24_RPD         0x09
#define NRF24_RX_ADDR_P0  0x0A
#define NRF24_RX_ADDR_P1  0x0B
#define NRF24_RX_ADDR_P2  0x0C
#define NRF24_RX_ADDR_P3  0x0D
#define NRF24_RX_ADDR_P4  0x0E
#define NRF24_RX_ADDR_P5  0x0F
#define NRF24_TX_ADDR     0x10
#define NRF24_RX_PW_P0    0x11
#define NRF24_RX_PW_P1    0x12
#define NRF24_RX_PW_P2    0x13
#define NRF24_RX_PW_P3    0x14
#define NRF24_RX_PW_P4    0x15
#define NRF24_RX_PW_P5    0x16
#define NRF24_FIFO_STATUS 0x17
#define NRF24_DYNPD       0x1C 
#define NRF24_FEATURE     0x1D


/* Registers bits */

// CONFIG
#define NRF24_MASK_RX_DR  6
#define NRF24_MASK_TX_DS  5
#define NRF24_MASK_MAX_RT 4
#define NRF24_EN_CRC      3
#define NRF24_CRCO        2
#define NRF24_PWR_UP      1
#define NRF24_PRIM_RX     0

// EN_AA
#define NRF24_ENAA_P5 5
#define NRF24_ENAA_P4 4
#define NRF24_ENAA_P3 3
#define NRF24_ENAA_P2 2
#define NRF24_ENAA_P1 1
#define NRF24_ENAA_P0 0

// EN_RXADDR
#define NRF24_ERX_P5 5
#define NRF24_ERX_P4 4
#define NRF24_ERX_P3 3
#define NRF24_ERX_P2 2
#define NRF24_ERX_P1 1
#define NRF24_ERX_P0 0 

// SETUP_AW
#define NRF24_AW 0

#define NRF24_SETUP_AW_3BYTES_ADDRESS (1 << NRF24_AW)
#define NRF24_SETUP_AW_4BYTES_ADDRESS (2 << NRF24_AW)
#define NRF24_SETUP_AW_5BYTES_ADDRESS (3 << NRF24_AW)

// SETUP_RETR 
#define NRF24_ARD 4
#define NRF24_ARC 0

#define NRF24_SETUP_RETR_DELAY_250MKS  (0 << NRF24_ARD)
#define NRF24_SETUP_RETR_DELAY_500MKS  (1 << NRF24_ARD)
#define NRF24_SETUP_RETR_DELAY_750MKS  (2 << NRF24_ARD)
#define NRF24_SETUP_RETR_DELAY_1000MKS (3 << NRF24_ARD)
#define NRF24_SETUP_RETR_DELAY_1250MKS (4 << NRF24_ARD)
#define NRF24_SETUP_RETR_DELAY_1500MKS (5 << NRF24_ARD)
#define NRF24_SETUP_RETR_DELAY_1750MKS (6 << NRF24_ARD)
#define NRF24_SETUP_RETR_DELAY_2000MKS (7 << NRF24_ARD)
#define NRF24_SETUP_RETR_DELAY_2250MKS (8 << NRF24_ARD)
#define NRF24_SETUP_RETR_DELAY_2500MKS (9 << NRF24_ARD)
#define NRF24_SETUP_RETR_DELAY_2750MKS (10 << NRF24_ARD)
#define NRF24_SETUP_RETR_DELAY_3000MKS (11 << NRF24_ARD)
#define NRF24_SETUP_RETR_DELAY_3250MKS (12 << NRF24_ARD)
#define NRF24_SETUP_RETR_DELAY_3500MKS (13 << NRF24_ARD)
#define NRF24_SETUP_RETR_DELAY_3750MKS (14 << NRF24_ARD)
#define NRF24_SETUP_RETR_DELAY_4000MKS (15 << NRF24_ARD)

#define NRF24_SETUP_RETR_NO_RETRANSMIT (0 << NRF24_ARC)
#define NRF24_SETUP_RETR_UP_TO_1_RETRANSMIT (1 << NRF24_ARC)
#define NRF24_SETUP_RETR_UP_TO_2_RETRANSMIT (2 << NRF24_ARC)
#define NRF24_SETUP_RETR_UP_TO_3_RETRANSMIT (3 << NRF24_ARC)
#define NRF24_SETUP_RETR_UP_TO_4_RETRANSMIT (4 << NRF24_ARC)
#define NRF24_SETUP_RETR_UP_TO_5_RETRANSMIT (5 << NRF24_ARC)
#define NRF24_SETUP_RETR_UP_TO_6_RETRANSMIT (6 << NRF24_ARC)
#define NRF24_SETUP_RETR_UP_TO_7_RETRANSMIT (7 << NRF24_ARC)
#define NRF24_SETUP_RETR_UP_TO_8_RETRANSMIT (8 << NRF24_ARC)
#define NRF24_SETUP_RETR_UP_TO_9_RETRANSMIT (9 << NRF24_ARC)
#define NRF24_SETUP_RETR_UP_TO_10_RETRANSMIT (10 << NRF24_ARC)
#define NRF24_SETUP_RETR_UP_TO_11_RETRANSMIT (11 << NRF24_ARC)
#define NRF24_SETUP_RETR_UP_TO_12_RETRANSMIT (12 << NRF24_ARC)
#define NRF24_SETUP_RETR_UP_TO_13_RETRANSMIT (13 << NRF24_ARC)
#define NRF24_SETUP_RETR_UP_TO_14_RETRANSMIT (14 << NRF24_ARC)
#define NRF24_SETUP_RETR_UP_TO_15_RETRANSMIT (15 << NRF24_ARC)

// RF_SETUP
#define NRF24_CONT_WAVE   7
#define NRF24_RF_DR_LOW   5
#define NRF24_PLL_LOCK    4
#define NRF24_RF_DR_HIGH  3
#define NRF24_RF_PWR      1

#define NRF24_RF_SETUP_MINUS18DBM (0 << NRF24_RF_PWR)
#define NRF24_RF_SETUP_MINUS12DBM (1 << NRF24_RF_PWR)
#define NRF24_RF_SETUP_MINUS6DBM  (2 << NRF24_RF_PWR)
#define NRF24_RF_SETUP_0DBM       (3 << NRF24_RF_PWR)

#define NRF24_RF_SETUP_1MBPS (0 << NRF24_RF_DR_HIGH)
#define NRF24_RF_SETUP_2MBPS (1 << NRF24_RF_DR_HIGH)
#define NRF24_RF_SETUP_250KBPS (1 << NRF24_RF_DR_LOW)

// STATUS
#define NRF24_RX_DR   6
#define NRF24_TX_DS   5
#define NRF24_MAX_RT  4
#define NRF24_RX_P_NO 1
#define NRF24_TX_FULL_STATUS 0

// OBSERVE_TX
#define NRF24_PLOS_CNT  4
#define NRF24_ARC_CNT   0

// FIFO_STATUS
#define NRF24_TX_REUSE      6
#define NRF24_TX_FULL_FIFO  5

#define NRF24_TX_EMPTY      4
#define NRF24_RX_FULL       1
#define NRF24_RX_EMPTY      0

// DYNDP
#define NRF24_DPL_P5 5
#define NRF24_DPL_P4 4
#define NRF24_DPL_P3 3
#define NRF24_DPL_P2 2
#define NRF24_DPL_P1 1
#define NRF24_DPL_P0 0

// FEATURE  
#define NRF24_EN_DPL      2
#define NRF24_EN_ACK_PAY  1
#define NRF24_EN_DYN_ACK  0


static uint8_t txBusyFlag = 0; // Only one package can be transmited in one time in this realization.
static uint8_t initializationFlag = 0;


static uint8_t NRF24_ReadRegisterToCmd(uint8_t reg);
static uint8_t NRF24_WriteRegisterToCmd(uint8_t reg);
static HAL_NRF24_ResultTypeDef HAL_NRF24_ReadCommand(uint8_t command, uint8_t *data, uint8_t length);
static HAL_NRF24_ResultTypeDef HAL_NRF24_WriteCommand(uint8_t command, uint8_t *data, uint8_t length);
static HAL_NRF24_ResultTypeDef HAL_NRF24_ReadRegister(uint8_t reg, uint8_t *data);
static HAL_NRF24_ResultTypeDef HAL_NRF24_WriteRegister(uint8_t reg, uint8_t data);
//static HAL_NRF24_ResultTypeDef HAL_NRF24_ReadRegisterData(uint8_t reg, uint8_t *data, uint8_t length);
static HAL_NRF24_ResultTypeDef HAL_NRF24_WriteRegisterData(uint8_t reg, uint8_t *data, uint8_t length);
static HAL_NRF24_ResultTypeDef HAL_NRF24_ExecuteCommand(uint8_t command);
static HAL_NRF24_ResultTypeDef HAL_NRF24_ReadStatus(uint8_t *status);
static NRF24_ResultTypeDef NRF24_HardwareError(void);
static NRF24_ResultTypeDef NRF24_FlushRxBuffer(void);
static NRF24_ResultTypeDef NRF24_FlushTxBuffer(void);


// Convert register to read register command.
static uint8_t NRF24_ReadRegisterToCmd(uint8_t reg)
{
    return ((reg & NRF24_REGISTER_MASK) | NRF24_R_REGISTER);
}


// Convert register to write register command.
static uint8_t NRF24_WriteRegisterToCmd(uint8_t reg)
{    
    return ((reg & NRF24_REGISTER_MASK) | NRF24_W_REGISTER);
}


// Write command, read *length* of bytes.
static HAL_NRF24_ResultTypeDef HAL_NRF24_ReadCommand(uint8_t command, uint8_t *data, uint8_t length)
{
    HAL_NRF24_ActivateCsn();
    
    HAL_NRF24_ResultTypeDef result = HAL_NRF24_Transmit(&command, 1);
    
    if (result == HAL_NRF24_ERR_OK)
    {
        result = HAL_NRF24_Receive(data, length);
    }
    
    HAL_NRF24_DeActivateCsn();    
    
    return result;
}


// Write command and *length* of bytes.
static HAL_NRF24_ResultTypeDef HAL_NRF24_WriteCommand(uint8_t command, uint8_t *data, uint8_t length)
{    
    HAL_NRF24_ActivateCsn();
    
    HAL_NRF24_ResultTypeDef result = HAL_NRF24_Transmit(&command, 1);
    
    if (result == HAL_NRF24_ERR_OK)
    {        
        result = HAL_NRF24_Transmit(data, length);
    }
    
    HAL_NRF24_DeActivateCsn();
    
    return result;
}


// Read one byte from register.
static HAL_NRF24_ResultTypeDef HAL_NRF24_ReadRegister(uint8_t reg, uint8_t *data)
{
    HAL_NRF24_ActivateCsn();
    
    uint8_t command = NRF24_ReadRegisterToCmd(reg);
    
    HAL_NRF24_ResultTypeDef result = HAL_NRF24_Transmit(&command, 1);
    
    if (result == HAL_NRF24_ERR_OK)
    {
        result = HAL_NRF24_Receive(data, 1);
    }
    
    HAL_NRF24_DeActivateCsn();    
    
    return result;
}


// Write one byte to register.
static HAL_NRF24_ResultTypeDef HAL_NRF24_WriteRegister(uint8_t reg, uint8_t data)
{
    HAL_NRF24_ActivateCsn();
    
    uint8_t command = NRF24_WriteRegisterToCmd(reg);
    
    HAL_NRF24_ResultTypeDef result = HAL_NRF24_Transmit(&command, 1);
    
    if (result == HAL_NRF24_ERR_OK)
    {
        result = HAL_NRF24_Transmit(&data, 1);
    }
    
    HAL_NRF24_DeActivateCsn();
    
    return result;
}

/*
// Read *length* of bytes from register.
static HAL_NRF24_ResultTypeDef HAL_NRF24_ReadRegisterData(uint8_t reg, uint8_t *data, uint8_t length)
{
    return HAL_NRF24_ReadCommand(NRF24_ReadRegisterToCmd(reg), data, length);
}
*/

// Write *length* of bytes to register.
static HAL_NRF24_ResultTypeDef HAL_NRF24_WriteRegisterData(uint8_t reg, uint8_t *data, uint8_t length)
{
    return HAL_NRF24_WriteCommand(NRF24_WriteRegisterToCmd(reg), data, length);
}


// Execute command.
static HAL_NRF24_ResultTypeDef HAL_NRF24_ExecuteCommand(uint8_t command)
{
    HAL_NRF24_ActivateCsn();
    
    HAL_NRF24_ResultTypeDef result = HAL_NRF24_Transmit(&command, 1);
    
    HAL_NRF24_DeActivateCsn();    
    
    return result;
}


// Read status.
static HAL_NRF24_ResultTypeDef HAL_NRF24_ReadStatus(uint8_t *status)
{
    HAL_NRF24_ActivateCsn();
    
    uint8_t command = NRF24_NOP;
    
    HAL_NRF24_ResultTypeDef result = HAL_NRF24_TransmitReceive(&command, status, 1);
    
    HAL_NRF24_DeActivateCsn();    
    
    return result;
}


// Break initialization, return result.
static NRF24_ResultTypeDef NRF24_HardwareError(void)
{
    initializationFlag = 0;
    
    HAL_NRF24_DeActivateCe();
    
    return NRF24_ERR_HARDWARE;
}


// Flush RX buffer.
static NRF24_ResultTypeDef NRF24_FlushRxBuffer(void)
{
    uint8_t status = 0;
    
    if (HAL_NRF24_ReadStatus(&status) != HAL_NRF24_ERR_OK) return NRF24_HardwareError();
    
    while (((status >> NRF24_RX_P_NO) & 7) != 7) // 3-bit value, 7 is mask 0b00000111. 7 mean buffer empty.
    {
        if (HAL_NRF24_ExecuteCommand(NRF24_FLUSH_RX) != HAL_NRF24_ERR_OK) return NRF24_HardwareError();  // Flush RX register.

        if (HAL_NRF24_ReadStatus(&status) != HAL_NRF24_ERR_OK) return NRF24_HardwareError();  // Read status after flush.
    }
    
    return NRF24_ERR_OK;
}


// Flush TX buffer.
static NRF24_ResultTypeDef NRF24_FlushTxBuffer()
{
    uint8_t fifoStatus = 0;
    
    if (HAL_NRF24_ReadRegister(NRF24_FIFO_STATUS, &fifoStatus) != HAL_NRF24_ERR_OK) return NRF24_HardwareError();
        
    while (!(fifoStatus & (1 << NRF24_TX_EMPTY)))
    {        
        if (HAL_NRF24_ExecuteCommand(NRF24_FLUSH_TX) != HAL_NRF24_ERR_OK) return NRF24_HardwareError();  // Flush TX register.

        if (HAL_NRF24_ReadRegister(NRF24_FIFO_STATUS, &fifoStatus) != HAL_NRF24_ERR_OK) return NRF24_HardwareError();  // Read fifo status after flush.
    }
    
    return NRF24_ERR_OK;
}


// Read width of payload in RX data register.
NRF24_ResultTypeDef NRF24_ReadRxPayloadWidth(uint8_t *width)
{
    HAL_NRF24_ActivateCsn();
    
    uint8_t command = NRF24_R_RX_PL_WID;
    
    HAL_NRF24_ResultTypeDef result = HAL_NRF24_Transmit(&command, 1);
    
    if (result == HAL_NRF24_ERR_OK)
    {
        result = HAL_NRF24_Receive(width, 1);
    }
    
    HAL_NRF24_DeActivateCsn();    
    
    return result == HAL_NRF24_ERR_OK ? NRF24_ERR_OK : NRF24_HardwareError();
}


// Start (or restart) NRF24 in transmit and receive mode.
NRF24_ResultTypeDef NRF24_Start()
{
    initializationFlag = 0;
    txBusyFlag = 0;

    HAL_NRF24_DeActivateCe();    
    
    #ifdef NRF24_MODE_TX_ONLY // Disable RX.
        const uint8_t initialConfig = (1 << NRF24_EN_CRC) | (1 << NRF24_CRCO);
    #else // Enable RX.
        const uint8_t initialConfig = (1 << NRF24_EN_CRC) | (1 << NRF24_CRCO) | (1 << NRF24_PRIM_RX);
    #endif
        
    for (uint8_t counter = NRF24_CONN_REPEAT_TIMES;;) // Try connect.
    {
        // Enable CRC16, enable receiver, power down.
        if (HAL_NRF24_WriteRegister(NRF24_CONFIG, initialConfig) != HAL_NRF24_ERR_OK) return NRF24_HardwareError();
        
        HAL_NRF24_DelayUs(NRF24_DIR_CHANGE_DELAY_US); // Direction change delay if direction changed.
        
        uint8_t config = 0;
        
        if (HAL_NRF24_ReadRegister(NRF24_CONFIG, &config) != HAL_NRF24_ERR_OK) return NRF24_HardwareError();
        
        if (config == initialConfig) break; // Connection succesfull.
        
        if (!counter--) return NRF24_HardwareError();
        
        HAL_NRF24_DelayMs(NRF24_CONN_DELAY_MS);
    }
    
    
    // Flush registers.
    if (NRF24_FlushRxBuffer() != NRF24_ERR_OK) return NRF24_HardwareError();
    if (NRF24_FlushTxBuffer() != NRF24_ERR_OK) return NRF24_HardwareError();
    
    
    uint8_t status = 0;
    
    if (HAL_NRF24_ReadStatus(&status) != HAL_NRF24_ERR_OK) return NRF24_HardwareError();
    
    if (HAL_NRF24_WriteRegister(NRF24_STATUS, status) != HAL_NRF24_ERR_OK) return NRF24_HardwareError();  // Write readed status back to clean interrupt flags.
    
    
    if (HAL_NRF24_WriteRegister(NRF24_SETUP_AW, NRF24_SETUP_AW_5BYTES_ADDRESS) != HAL_NRF24_ERR_OK) return NRF24_HardwareError(); // 5 bytes address length.
    
    if (HAL_NRF24_WriteRegister(NRF24_SETUP_RETR, NRF24_SETUP_RETR_DELAY_250MKS | NRF24_SETUP_RETR_UP_TO_2_RETRANSMIT) != HAL_NRF24_ERR_OK) return NRF24_HardwareError();
    
    if (HAL_NRF24_WriteRegister(NRF24_RF_CH, NRF24_FREQ) != HAL_NRF24_ERR_OK) return NRF24_HardwareError(); // Select frequency channel.
    
    if (HAL_NRF24_WriteRegister(NRF24_RF_SETUP, NRF24_RF_SETUP_1MBPS | NRF24_RF_SETUP_0DBM) != HAL_NRF24_ERR_OK) return NRF24_HardwareError(); // 1 Mbit/s and 0 dBm.
    
    if (HAL_NRF24_WriteRegister(NRF24_FEATURE, (1 << NRF24_EN_DPL)) != HAL_NRF24_ERR_OK) return NRF24_HardwareError(); // Enable dynamic length.
    
    
    #ifdef NRF24_MODE_RX_TX
        const uint8_t enaaValue = (1 << NRF24_ENAA_P1);
        const uint8_t dinpdValue = (1 << NRF24_DPL_P0) | (1 << NRF24_DPL_P1);
        const uint8_t enrxaddrValue = (1 << NRF24_ERX_P0) | (1 << NRF24_ERX_P1);
        
        if (HAL_NRF24_WriteRegister(NRF24_RX_PW_P0, 0) != HAL_NRF24_ERR_OK) return NRF24_HardwareError(); // Input data length in pipe 0.
        
        if (HAL_NRF24_WriteRegister(NRF24_RX_PW_P1, 32) != HAL_NRF24_ERR_OK) return NRF24_HardwareError(); // Input data length in pipe 1.
        
        #ifdef NRF24_RXTX_DIRECT
            uint8_t remoteAddress[5] = { NRF24_TX_BYTE, NRF24_TX_BYTE, NRF24_TX_BYTE, NRF24_TX_BYTE, NRF24_TX_BYTE };
            uint8_t selfAddress[5] = { NRF24_RX_BYTE, NRF24_RX_BYTE, NRF24_RX_BYTE, NRF24_RX_BYTE, NRF24_RX_BYTE };
        #else // Reverse.
            uint8_t remoteAddress[5] = { NRF24_RX_BYTE, NRF24_RX_BYTE, NRF24_RX_BYTE, NRF24_RX_BYTE, NRF24_RX_BYTE };
            uint8_t selfAddress[5] = { NRF24_TX_BYTE, NRF24_TX_BYTE, NRF24_TX_BYTE, NRF24_TX_BYTE, NRF24_TX_BYTE };
        #endif
        
        if (HAL_NRF24_WriteRegisterData(NRF24_RX_ADDR_P0, remoteAddress, 5) != HAL_NRF24_ERR_OK) return NRF24_HardwareError(); // Set TX address.
        
        if (HAL_NRF24_WriteRegisterData(NRF24_TX_ADDR, remoteAddress, 5) != HAL_NRF24_ERR_OK) return NRF24_HardwareError(); // Set TX address.
        
        if (HAL_NRF24_WriteRegisterData(NRF24_RX_ADDR_P1, selfAddress, 5) != HAL_NRF24_ERR_OK) return NRF24_HardwareError(); // Set RX address.
    #endif
    
    #ifdef NRF24_MODE_TX_ONLY
        const uint8_t enaaValue = 0;
        const uint8_t dinpdValue = (1 << NRF24_DPL_P0);
        const uint8_t enrxaddrValue = 0;        
        
        if (HAL_NRF24_WriteRegister(NRF24_RX_PW_P0, 0) != HAL_NRF24_ERR_OK) return NRF24_HardwareError(); // Input data length in pipe 0.
        
        uint8_t remoteAddress[5] = { NRF24_TX_BYTE, NRF24_TX_BYTE, NRF24_TX_BYTE, NRF24_TX_BYTE, NRF24_TX_LAST };
        
        if (HAL_NRF24_WriteRegisterData(NRF24_RX_ADDR_P0, remoteAddress, 5) != HAL_NRF24_ERR_OK) return NRF24_HardwareError(); // Set TX address.
        
        if (HAL_NRF24_WriteRegisterData(NRF24_TX_ADDR, remoteAddress, 5) != HAL_NRF24_ERR_OK) return NRF24_HardwareError(); // Set TX address.
    #endif
    
    #ifdef NRF24_MODE_RX_ONLY
        uint8_t enaaValue = 0;
        uint8_t dinpdValue = 0;
        uint8_t enrxaddrValue = 0;        
        
        uint8_t selfAddress[5] = { NRF24_RX_BYTE, NRF24_RX_BYTE, NRF24_RX_BYTE, NRF24_RX_BYTE, NRF24_PIPE_1 }; // 4 bytes of RX address of pipe 1 used for pipes 2-5.
                
        if (HAL_NRF24_WriteRegisterData(NRF24_RX_ADDR_P1, selfAddress, 5) != HAL_NRF24_ERR_OK) return NRF24_HardwareError(); // Set 5-byte RX address of pipe 1.
        
        #ifdef NRF24_USE_PIPE_1        
            enaaValue |= (1 << NRF24_ENAA_P1);
            dinpdValue |= (1 << NRF24_DPL_P1);
            enrxaddrValue |= (1 << NRF24_ERX_P1);
                
            if (HAL_NRF24_WriteRegister(NRF24_RX_PW_P1, 32) != HAL_NRF24_ERR_OK) return NRF24_HardwareError(); // Input data length in pipe 1.
        #endif
        #ifdef NRF24_USE_PIPE_2        
            enaaValue | = (1 << NRF24_ENAA_P2);
            dinpdValue |= (1 << NRF24_DPL_P2);
            enrxaddrValue |= (1 << NRF24_ERX_P2);
                
            if (HAL_NRF24_WriteRegister(NRF24_RX_PW_P2, 32) != HAL_NRF24_ERR_OK) return NRF24_HardwareError(); // Input data length in pipe2.
                
            if (HAL_NRF24_WriteRegister(NRF24_RX_ADDR_P2, NRF24_PIPE_2) != HAL_NRF24_ERR_OK) return NRF24_HardwareError(); // Set 1-byte RX address of pipe 2.
        #endif
        #ifdef NRF24_USE_PIPE_3    
            enaaValue | = (1 << NRF24_ENAA_P3);
            dinpdValue |= (1 << NRF24_DPL_P3);
            enrxaddrValue |= (1 << NRF24_ERX_P3);
                
            if (HAL_NRF24_WriteRegister(NRF24_RX_PW_P3, 32) != HAL_NRF24_ERR_OK) return NRF24_HardwareError(); // Input data length in pipe 3.

            if (HAL_NRF24_WriteRegister(NRF24_RX_ADDR_P3, NRF24_PIPE_3) != HAL_NRF24_ERR_OK) return NRF24_HardwareError(); // Set 1-byte RX address of pipe 3.
        #endif
        #ifdef NRF24_USE_PIPE_4        
            enaaValue | = (1 << NRF24_ENAA_P4);
            dinpdValue |= (1 << NRF24_DPL_P4);
            enrxaddrValue |= (1 << NRF24_ERX_P4);
                
            if (HAL_NRF24_WriteRegister(NRF24_RX_PW_P4, 32) != HAL_NRF24_ERR_OK) return NRF24_HardwareError(); // Input data length in pipe 4.

            if (HAL_NRF24_WriteRegister(NRF24_RX_ADDR_P4, NRF24_PIPE_4) != HAL_NRF24_ERR_OK) return NRF24_HardwareError(); // Set 1-byte RX address of pipe 4.
        #endif
        #ifdef NRF24_USE_PIPE_5
            enaaValue | = (1 << NRF24_ENAA_P5);
            dinpdValue |= (1 << NRF24_DPL_P5);
            enrxaddrValue |= (1 << NRF24_ERX_P5);
                
            if (HAL_NRF24_WriteRegister(NRF24_RX_PW_P5, 32) != HAL_NRF24_ERR_OK) return NRF24_HardwareError(); // Input data length in pipe 5.

            if (HAL_NRF24_WriteRegister(NRF24_RX_ADDR_P5, NRF24_PIPE_5) != HAL_NRF24_ERR_OK) return NRF24_HardwareError(); // Set 1-byte RX address of pipe 5.
        #endif
    #endif
    
    
    if (HAL_NRF24_WriteRegister(NRF24_EN_AA, enaaValue) != HAL_NRF24_ERR_OK) return NRF24_HardwareError(); // Enable auto ACK.
    
    if (HAL_NRF24_WriteRegister(NRF24_DYNPD, dinpdValue) != HAL_NRF24_ERR_OK) return NRF24_HardwareError(); // Enable dynamic data length.
    
    if (HAL_NRF24_WriteRegister(NRF24_EN_RXADDR, enrxaddrValue) != HAL_NRF24_ERR_OK) return NRF24_HardwareError(); // Enable RX channels.
                
    
    // Enable CRC16, enable receiver, power up.
    if (HAL_NRF24_WriteRegister(NRF24_CONFIG, (initialConfig | (1 << NRF24_PWR_UP))) != HAL_NRF24_ERR_OK) return NRF24_HardwareError();
        
    HAL_NRF24_DelayMs(NRF24_POWER_DELAY_MS);  // Power up delay (NRF24_POWER_DELAY_MS).
    
    uint8_t config = 0;
    
    if (HAL_NRF24_ReadRegister(NRF24_CONFIG, &config) != HAL_NRF24_ERR_OK) return NRF24_HardwareError();
    
    initializationFlag = 1;
    
    #ifndef NRF24_MODE_TX_ONLY // Enable receiving.
        HAL_NRF24_ActivateCe();
    #endif
        
    return (config == (initialConfig | (1 << NRF24_PWR_UP))) ? NRF24_ERR_OK : NRF24_HardwareError(); // Start succesful.
}


#if defined(NRF24_MODE_TX_ONLY) || defined(NRF24_MODE_RX_TX)
// Send data.
NRF24_ResultTypeDef NRF24_Send(uint8_t *data, uint8_t length)
{
    if (!initializationFlag) return NRF24_ERR_NOINIT;
    
    if (txBusyFlag) return NRF24_ERR_TXFULL;
    txBusyFlag = 1;
        
    if (length > NRF24_DATA_MAX_LENGTH) return NRF24_ERR_WRONGLENGTH;
        
    HAL_NRF24_DeActivateCe(); // Disable receiving if in RX mode.
        
    uint8_t config = 0;
    
    if (HAL_NRF24_ReadRegister(NRF24_CONFIG, &config) != HAL_NRF24_ERR_OK) return NRF24_HardwareError();
    
    
    #ifdef NRF24_MODE_TX_ONLY // In this mode power can be down.
        if (!(config & (1 << NRF24_PWR_UP))) // Power up if need.
        {
            if (HAL_NRF24_WriteRegister(NRF24_CONFIG, (config | (1 << NRF24_PWR_UP))) != HAL_NRF24_ERR_OK) return NRF24_HardwareError();
                
            HAL_NRF24_DelayMs(NRF24_POWER_DELAY_MS);  // Power up delay (NRF24_POWER_DELAY_MS).
        }
    #endif
            
            
    if (!(config & (1 << NRF24_PWR_UP))) return NRF24_ERR_PWRDOWN; // If power is down, return error.
    
    if (HAL_NRF24_WriteRegister(NRF24_CONFIG, config & ~(1 << NRF24_PRIM_RX)) != HAL_NRF24_ERR_OK) return NRF24_HardwareError(); // Switch to TX mode.
        
    HAL_NRF24_DelayUs(NRF24_DIR_CHANGE_DELAY_US); // Direction change delay (NRF24_PRIM_RX).
    
    if (HAL_NRF24_WriteCommand(NRF24_W_TX_PAYLOAD, data, length) != HAL_NRF24_ERR_OK) return NRF24_HardwareError(); // Add data to TX buffer.
        
    HAL_NRF24_ActivateCe(); // Start transmission.
    HAL_NRF24_DelayUs(NRF24_TX_DELAY_US);
    HAL_NRF24_DeActivateCe();
        
    return NRF24_ERR_OK;
}
#endif /* defined(NRF24_MODE_TX_ONLY) || defined(NRF24_MODE_RX_TX) */


#if defined(NRF24_MODE_RX_ONLY) || defined(NRF24_MODE_RX_TX)
// Receive data.
NRF24_ResultTypeDef NRF24_Receive(uint8_t *data, uint8_t length, uint8_t *pipe)
{
    if (!initializationFlag) return NRF24_ERR_NOINIT;
    
    uint8_t status = 0;
    
    if (HAL_NRF24_ReadStatus(&status) != HAL_NRF24_ERR_OK) return NRF24_HardwareError();
        
    uint8_t pipeValue = ((status >> NRF24_RX_P_NO) & 7);// 3-bit value, 7 is mask 0b00000111. 7 mean buffer empty, if !7 - number of pipe.
    if (pipeValue > 5) return NRF24_ERR_RXEMPTY; // Return if RX buffer is empty.
    
    *pipe = pipeValue;
        
        
    uint8_t flushFlag = 0;
        
    #ifdef NRF24_MODE_RX_TX
        if (pipeValue != 1) flushFlag = 1;
    #endif
    #ifdef NRF24_MODE_RX_ONLY
        #ifndef NRF24_USE_PIPE_1
            if (pipeValue == 1) flushFlag = 1;
        #endif
        #ifndef NRF24_USE_PIPE_2
            if (pipeValue == 2) flushFlag = 1;
        #endif
        #ifndef NRF24_USE_PIPE_3
            if (pipeValue == 3) flushFlag = 1;
        #endif
        #ifndef NRF24_USE_PIPE_4
            if (pipeValue == 4) flushFlag = 1;
        #endif
        #ifndef NRF24_USE_PIPE_5
            if (pipeValue == 5) flushFlag = 1;
        #endif
    #endif
        
    if (flushFlag)
    {        
        return (HAL_NRF24_ExecuteCommand(NRF24_FLUSH_RX) != HAL_NRF24_ERR_OK) ? NRF24_HardwareError() : NRF24_ERR_RXEMPTY; // Flush RX register, return RXEMPTY.
    }
        
        
    uint8_t width = 0; // Width of data in RX buffer.
    
    if (NRF24_ReadRxPayloadWidth(&width) != NRF24_ERR_OK) return NRF24_HardwareError();
        
    if (width > NRF24_DATA_MAX_LENGTH) // RX error.
    {
        if (HAL_NRF24_ExecuteCommand(NRF24_FLUSH_RX) != HAL_NRF24_ERR_OK) return NRF24_HardwareError(); // Flush RX register, return RXEMPTY.
    }
        
    if (width > length) return NRF24_ERR_WRONGLENGTH;
    
    return (HAL_NRF24_ReadCommand(NRF24_R_RX_PAYLOAD, data, length)) ? NRF24_HardwareError() : NRF24_ERR_OK;
}
#endif /* defined(NRF24_MODE_RX_ONLY) || defined(NRF24_MODE_RX_TX) */


// Process NRF24.
NRF24_ResultTypeDef NRF24_Process(NRF24_StateTypeDef *state)
{
    if (!initializationFlag) return NRF24_ERR_NOINIT;
    
    if (!HAL_NRF24_IsIrq())
    {
        *state = NRF24_STATE_NOIRQ;
        
        return NRF24_ERR_OK; // No IRQ, return.
    }
    
    uint8_t status = 0;
    
    if (HAL_NRF24_ReadStatus(&status) != HAL_NRF24_ERR_OK) return NRF24_HardwareError();
    
    if (HAL_NRF24_WriteRegister(NRF24_STATUS, status) != HAL_NRF24_ERR_OK) return NRF24_HardwareError(); // Write readed status back to clean interrupt flags.
    
    #if defined(NRF24_MODE_TX_ONLY) || defined(NRF24_MODE_RX_TX)
        if (status & (1 << NRF24_MAX_RT)) // No ACK.
        {
            txBusyFlag = 0;
            *state |= NRF24_STATE_NOACK;
            
            if (HAL_NRF24_ExecuteCommand(NRF24_FLUSH_TX) != HAL_NRF24_ERR_OK) return NRF24_HardwareError(); // Clean byte in TX buffer.
        }
        
        if (status & (1 << NRF24_TX_DS)) // TX is completed succesfully.
        {
            txBusyFlag = 0;
            *state |= NRF24_STATE_TXCOMPLETE;
        }
        
        #ifdef NRF24_MODE_RX_TX // Back to RX mode.    
            uint8_t config = 0;
            
            if (HAL_NRF24_ReadRegister(NRF24_CONFIG, &config) != HAL_NRF24_ERR_OK) return NRF24_HardwareError(); // Read config.
            
            if (HAL_NRF24_WriteRegister(NRF24_CONFIG, (config | (1 << NRF24_PRIM_RX))) != HAL_NRF24_ERR_OK) return NRF24_HardwareError(); // Write readed config back with RX primary flag to enable RX.
                
            HAL_NRF24_DelayUs(NRF24_DIR_CHANGE_DELAY_US); // Direction change delay (NRF24_PRIM_RX).

            HAL_NRF24_ActivateCe(); // Enable receiving.
        #endif
        
        #ifdef NRF24_MODE_TX_ONLY // In TX only power can be disabled after TX.
            uint8_t config = 0;
            
            if (HAL_NRF24_ReadRegister(NRF24_CONFIG, &config) != HAL_NRF24_ERR_OK) return NRF24_HardwareError();

            if (config & (1 << NRF24_PWR_UP)) // If power up.
            {
                if (HAL_NRF24_WriteRegister(NRF24_CONFIG, (config & ~(1 << NRF24_PWR_UP))) != HAL_NRF24_ERR_OK) return NRF24_HardwareError(); // Then power down.
            }
        #endif
    #endif /* defined(NRF24_MODE_TX_ONLY) || defined(NRF24_MODE_RX_TX) */

    if (status & (1 << NRF24_RX_DR)) // Package is received succesfully.
    {
        #if (defined(NRF24_MODE_RX_ONLY) || defined(NRF24_MODE_RX_TX))
            *state |= NRF24_STATE_RXCOMPLETE; // All receiving in Receive function, here only flag.
        #else // If receiving is disabled, flush RX buffer.
            if (NRF24_FlushRxBuffer() != NRF24_ERR_OK) return NRF24_HardwareError();
        #endif
    }
    
    return NRF24_ERR_OK;
}


// Flag that indicates initialization state.
uint8_t NRF24_IsInitialized()
{
    return initializationFlag;
}


// Flag that indicates TX buffer state.
uint8_t NRF24_IsTXBusy()
{
    return txBusyFlag;
}
