#ifndef __NRF24_HAL_H
#define __NRF24_HAL_H


#include "stm32f0xx_hal.h"


#define HAL_NRF24_SPI_TIMEOUT 1000

#define HAL_NRF24_CE_Pin GPIO_PIN_1
#define HAL_NRF24_CE_GPIO_Port GPIOA
#define HAL_NRF24_CSN_Pin GPIO_PIN_4
#define HAL_NRF24_CSN_GPIO_Port GPIOA
#define HAL_NRF24_IRQ_Pin GPIO_PIN_1
#define HAL_NRF24_IRQ_GPIO_Port GPIOB


typedef enum Results
{
    HAL_NRF24_ERR_OK,
    HAL_NRF24_ERR_SPI,
    HAL_NRF24_ERR_NOANSWER
}
HAL_NRF24_ResultTypeDef;


extern SPI_HandleTypeDef hspi1;


static HAL_NRF24_ResultTypeDef HAL_NRF24_Init(void);

static HAL_NRF24_ResultTypeDef HAL_NRF24_Transmit(uint8_t *pTxData, uint8_t length);
static HAL_NRF24_ResultTypeDef HAL_NRF24_Receive(uint8_t *pRxData, uint8_t length);
static HAL_NRF24_ResultTypeDef HAL_NRF24_TransmitReceive(uint8_t *pTxData, uint8_t *pRxData, uint8_t length);
static void HAL_NRF24_ActivateCe(void);
static void HAL_NRF24_DeActivateCe(void);
static void HAL_NRF24_ActivateCsn(void);
static void HAL_NRF24_DeActivateCsn(void);
static uint8_t HAL_NRF24_IsIrq(void);
static void HAL_NRF24_DelayMs(uint8_t ms);
static void HAL_NRF24_DelayUs(uint8_t us);


// SPI initialized in main.
static HAL_NRF24_ResultTypeDef HAL_NRF24_Init()
{
    HAL_NRF24_DeActivateCsn();
    HAL_NRF24_DeActivateCe();
    
    return HAL_NRF24_ERR_OK;
}

static HAL_NRF24_ResultTypeDef HAL_NRF24_Transmit(uint8_t *pTxData, uint8_t length)
{
    uint8_t pRxData[length];
    
    return (HAL_SPI_TransmitReceive(&hspi1, pTxData, pRxData, length, HAL_NRF24_SPI_TIMEOUT) != HAL_OK) ? HAL_NRF24_ERR_SPI : HAL_NRF24_ERR_OK;
}

static HAL_NRF24_ResultTypeDef HAL_NRF24_Receive(uint8_t *pRxData, uint8_t length)
{
    return (HAL_SPI_TransmitReceive(&hspi1, pRxData, pRxData, length, HAL_NRF24_SPI_TIMEOUT) != HAL_OK) ? HAL_NRF24_ERR_SPI : HAL_NRF24_ERR_OK;
}

static HAL_NRF24_ResultTypeDef HAL_NRF24_TransmitReceive(uint8_t *pTxData, uint8_t *pRxData, uint8_t length)
{
    return (HAL_SPI_TransmitReceive(&hspi1, pTxData, pRxData, length, HAL_NRF24_SPI_TIMEOUT) != HAL_OK) ? HAL_NRF24_ERR_SPI : HAL_NRF24_ERR_OK;
}

// Active high.
static void HAL_NRF24_ActivateCe()
{
    HAL_GPIO_WritePin(HAL_NRF24_CE_GPIO_Port, HAL_NRF24_CE_Pin, GPIO_PIN_SET);
}

// Active high.
static void HAL_NRF24_DeActivateCe()
{
    HAL_GPIO_WritePin(HAL_NRF24_CE_GPIO_Port, HAL_NRF24_CE_Pin, GPIO_PIN_RESET);
}

// Active low.
static void HAL_NRF24_ActivateCsn()
{
    HAL_GPIO_WritePin(HAL_NRF24_CSN_GPIO_Port, HAL_NRF24_CSN_Pin, GPIO_PIN_RESET);
}

// Active low.
static void HAL_NRF24_DeActivateCsn()
{
    HAL_GPIO_WritePin(HAL_NRF24_CSN_GPIO_Port, HAL_NRF24_CSN_Pin, GPIO_PIN_SET);
}

// Active low.
static uint8_t HAL_NRF24_IsIrq()
{
    return (HAL_GPIO_ReadPin(HAL_NRF24_IRQ_GPIO_Port, HAL_NRF24_IRQ_Pin) == GPIO_PIN_RESET) ? 1 : 0;
}

// Delay in milliseconds, use SysTick.
static void HAL_NRF24_DelayMs(uint8_t ms)
{
    HAL_Delay(ms);
}

// Delay in microseconds, use software delay.
static void HAL_NRF24_DelayUs(uint8_t us)
{
    volatile uint32_t delay = (uint32_t)((HAL_RCC_GetSysClockFreq() / (float)8000000) * us); // Not good, but works.
    
    while (delay--);
}


#endif /* __NRF24_HAL_H */
