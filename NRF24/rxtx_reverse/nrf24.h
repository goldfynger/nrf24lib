#ifndef __NRF24_H
#define __NRF24_H


#include "stdint.h"


/* Type defines */

typedef enum NRF24_States
{
    NRF24_STATE_NOIRQ = 0, // No action needed.
    NRF24_STATE_TXCOMPLETE = 1, // Tx complete flag.
    NRF24_STATE_NOACK = 2, // Tx no ACK.
    NRF24_STATE_RXCOMPLETE = 4 // Rx complete, need read data.
}
NRF24_StateTypeDef;

typedef enum NRF24_Results
{
    NRF24_ERR_OK,
    NRF24_ERR_NOINIT, // Initialization (or reinitialization after error) required.
    NRF24_ERR_HARDWARE, // SPI or NRF24 initialization error. NRF24 restart required.
    NRF24_ERR_PWRDOWN, // Power bit is cleared. Need restart NRF24 (or try send data again in TX-only mode).
    NRF24_ERR_TXFULL, // TX already in buffer.
    NRF24_ERR_RXEMPTY, // RX buffer empty or RX error.
    NRF24_ERR_WRONGLENGTH // Length of buffer is wrong.
}
NRF24_ResultTypeDef;


/* Settings */

//#define NRF24_MODE_TX_ONLY // TX-only mode. 0-pipe used for ACK receiving. Power normally down, up in send func and down back when tx complete.
//#define NRF24_MODE_RX_ONLY // RX-only mode. 1-5 pipes are used. (1 always, 2-5 if need). 1 RX-only device, up to 5 TX-only devices.
#define NRF24_MODE_RX_TX // TX-RX mode. 0-pipe used for ACK receiving, 1-pipe used for data receiving. Only two devices, each RX-TX.


#ifdef NRF24_MODE_RX_TX    
    //#define NRF24_RXTX_DIRECT
    #define NRF24_RXTX_REVERSE

    #define NRF24_TX_BYTE 0xE7
    #define NRF24_RX_BYTE 0xC2
#endif


#if (defined(NRF24_MODE_TX_ONLY) || defined(NRF24_MODE_RX_ONLY))
    #define NRF24_PIPE_0 0xE7
    #define NRF24_PIPE_1 0xC2
    #define NRF24_PIPE_2 0xC3
    #define NRF24_PIPE_3 0xC4
    #define NRF24_PIPE_4 0xC5
    #define NRF24_PIPE_5 0xC6
#endif


#ifdef NRF24_MODE_TX_ONLY
    #define NRF24_TX_BYTE NRF24_PIPE_1

    #define NRF24_DEV_1 // TX to pipe 1.
    //#define NRF24_DEV_2 // TX to pipe 2.
    //#define NRF24_DEV_3 // TX to pipe 3.
    //#define NRF24_DEV_4 // TX to pipe 4.
    //#define NRF24_DEV_5 // TX to pipe 5.

    #define NRF24_RX_BYTE NRF24_PIPE_0 // RX not used in this mode, but value must be set.
#endif


#ifdef NRF24_MODE_RX_ONLY
    #define NRF24_RX_BYTE NRF24_PIPE_1
    
    #define NRF24_USE_PIPE_1
    //#define NRF24_USE_PIPE_2
    //#define NRF24_USE_PIPE_3
    //#define NRF24_USE_PIPE_4
    //#define NRF24_USE_PIPE_5

    #define NRF24_TX_BYTE NRF24_PIPE_0 // TX not used in this mode, but value must be set.
#endif


#define NRF24_FREQ 3 // NRF24 frequency, 0..125 (2400..2525 MHz).


#if defined(NRF24_MODE_TX_ONLY) || defined(NRF24_MODE_RX_TX)
NRF24_ResultTypeDef NRF24_Send(uint8_t *data, uint8_t length); // Send data.
#endif

#if defined(NRF24_MODE_RX_ONLY) || defined(NRF24_MODE_RX_TX)
NRF24_ResultTypeDef NRF24_Receive(uint8_t *data, uint8_t length, uint8_t *pipe); // Receive data.
#endif

NRF24_ResultTypeDef NRF24_ReadRxPayloadWidth(uint8_t *width); // Read width of data in receive buffer.
NRF24_ResultTypeDef NRF24_Start(void); // Start (or restart) NRF24 in transmit and receive mode.
NRF24_ResultTypeDef NRF24_Process(NRF24_StateTypeDef *state); // Process NRF24. Use state pointer to determinate NRF24 state.
uint8_t NRF24_IsInitialized(void);
uint8_t NRF24_IsTXBusy(void);


#endif /* __NRF24_H */
