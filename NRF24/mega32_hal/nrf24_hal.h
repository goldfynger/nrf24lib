#ifndef __NRF24_HAL_H
#define __NRF24_HAL_H


#include <avr/io.h>
#include <util/delay.h>


#define HAL_NRF24_IRQ_PORT PORTD
#define HAL_NRF24_IRQ_DDR DDRD
#define HAL_NRF24_IRQ_PIN PIND
#define HAL_NRF24_IRQ 2

#define HAL_NRF24_CE_PORT PORTD
#define HAL_NRF24_CE_DDR DDRD
#define HAL_NRF24_CE_PIN PIND
#define HAL_NRF24_CE 3

#define HAL_NRF24_CSN_PORT PORTD
#define HAL_NRF24_CSN_DDR DDRD
#define HAL_NRF24_CSN_PIN PIND
#define HAL_NRF24_CSN 4

#define HAL_NRF24_SPI_DDR DDRB

#define HAL_NRF24_SS 4
#define HAL_NRF24_MOSI 5
#define HAL_NRF24_MISO 6
#define HAL_NRF24_SCK 7


typedef enum Results
{
    HAL_NRF24_ERR_OK,
    HAL_NRF24_ERR_SPI,
    HAL_NRF24_ERR_NOANSWER
}
HAL_NRF24_ResultTypeDef;


static HAL_NRF24_ResultTypeDef HAL_NRF24_Init(void);

static HAL_NRF24_ResultTypeDef HAL_NRF24_Transmit(uint8_t *pTxData, uint8_t length);
static HAL_NRF24_ResultTypeDef HAL_NRF24_Receive(uint8_t *pRxData, uint8_t length);
static HAL_NRF24_ResultTypeDef HAL_NRF24_TransmitReceive(uint8_t *pTxData, uint8_t *pRxData, uint8_t length);
static void HAL_NRF24_ActivateCe(void);
static void HAL_NRF24_DeActivateCe(void);
static void HAL_NRF24_ActivateCsn(void);
static void HAL_NRF24_DeActivateCsn(void);
static uint8_t HAL_NRF24_IsIrq(void);
static void HAL_NRF24_DelayMs(uint8_t ms);
static void HAL_NRF24_DelayUs(uint8_t us);


static HAL_NRF24_ResultTypeDef HAL_NRF24_Init()
{
    HAL_NRF24_IRQ_DDR &= ~(1 << HAL_NRF24_IRQ);
    HAL_NRF24_CE_DDR |= (1 << HAL_NRF24_CE);
    HAL_NRF24_CSN_DDR |= (1 << HAL_NRF24_CSN);

    HAL_NRF24_IRQ_PORT &= ~(1 << HAL_NRF24_IRQ);
    HAL_NRF24_DeActivateCe();
    HAL_NRF24_DeActivateCsn();    

    HAL_NRF24_SPI_DDR |= (1 << HAL_NRF24_SS) | (1 <<  HAL_NRF24_MOSI) | (1 <<  HAL_NRF24_SCK);
      SPCR = (1 << SPE) | (1 << MSTR) | (1 << SPR0); // Enable, master, divider 16.
    
    return HAL_NRF24_ERR_OK;
}

static HAL_NRF24_ResultTypeDef HAL_NRF24_Transmit(uint8_t *pTxData, uint8_t length)
{
    uint8_t rxBuffer;

    for (uint8_t i = 0; i < length; i++)
    {
        SPDR = pTxData[i];

        while (!(SPSR & (1 << SPIF)));

        rxBuffer = SPDR;
    }
    
    return HAL_NRF24_ERR_OK;
}

static HAL_NRF24_ResultTypeDef HAL_NRF24_Receive(uint8_t *pRxData, uint8_t length)
{
    for (uint8_t i = 0; i < length; i++)
    {
        SPDR = pRxData[i];

        while (!(SPSR & (1 << SPIF)));

        pRxData[i] = SPDR;
    }
    
    return HAL_NRF24_ERR_OK;
}

static HAL_NRF24_ResultTypeDef HAL_NRF24_TransmitReceive(uint8_t *pTxData, uint8_t *pRxData, uint8_t length)
{
    for (uint8_t i = 0; i < length; i++)
    {
        SPDR = pTxData[i];

        while (!(SPSR & (1 << SPIF)));

        pRxData[i] = SPDR;
    }
    
    return HAL_NRF24_ERR_OK;
}

// Active high.
static void HAL_NRF24_ActivateCe()
{
  HAL_NRF24_CE_PORT |= (1 << HAL_NRF24_CE);
}

// Active high.
static void HAL_NRF24_DeActivateCe()
{
  HAL_NRF24_CE_PORT &= ~(1 << HAL_NRF24_CE);
}

// Active low.
static void HAL_NRF24_ActivateCsn()
{
  HAL_NRF24_CSN_PORT &= ~(1 << HAL_NRF24_CSN);
}

// Active low.
static void HAL_NRF24_DeActivateCsn()
{
  HAL_NRF24_CSN_PORT |= (1 << HAL_NRF24_CSN);
}

// Active low.
static uint8_t HAL_NRF24_IsIrq()
{
    return !(HAL_NRF24_IRQ_PIN & (1 << HAL_NRF24_IRQ));
}

// _delay_ms requred constant, use while loop.
static void HAL_NRF24_DelayMs(uint8_t ms)
{
    while (ms--)
        _delay_ms(1);
}

// _delay_us requred constant, use while loop.
static void HAL_NRF24_DelayUs(uint8_t us)
{
    while (us--)
        _delay_us(1);
}


#endif /* __NRF24_HAL_H */
